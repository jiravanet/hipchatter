﻿using Matrix;
using Matrix.Xmpp.Client;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat.Model
{
    public class Contact : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public Chat chat;

        public Contact()
        {
            OnlineState = OnlineState.Offline;
        }

        public string Name { get; set; }
        public string Jid { get; set; }
        public string Status { get; set; }
        public string MentionName { get; set; }
        public string Mobile { get; set; }
        public OnlineState OnlineState { get; set; }

        /// <summary>
        /// Returns the online state, indicating also if the user is available on mobile
        /// </summary>
        public OnlineState ExtendedOnlineState
        {
            get
            {
                if (OnlineState == OnlineState.Offline)
                {
                    if (Mobile == "iphone") return OnlineState.OnMobileApple;
                    if (Mobile == "android") return OnlineState.OnMobileAndroid;
                }

                return OnlineState;
            }
        }

        public void SetOnlineStateFromPresence(Presence pres)
        {
            if (pres.Type == Matrix.Xmpp.PresenceType.unavailable)
                OnlineState = OnlineState.Offline;

            if (pres.Type == Matrix.Xmpp.PresenceType.available)
            {
                if (pres.Show == Matrix.Xmpp.Show.chat)
                    OnlineState = OnlineState.Chat;
                else if (pres.Show == Matrix.Xmpp.Show.away)
                    OnlineState = OnlineState.Away;
                else if (pres.Show == Matrix.Xmpp.Show.xa)
                    OnlineState = OnlineState.ExtendedAway;
                else if (pres.Show == Matrix.Xmpp.Show.dnd)
                    OnlineState = OnlineState.DoNotDisturb;
                else
                    OnlineState = OnlineState.Online;
            }

            Status = pres.Status ?? "";
            OnPropertyChanged("Status");
            OnPropertyChanged("OnlineState");
            OnPropertyChanged("ExtendedOnlineState");
        }

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
