﻿using HipChat.Libs;
using Matrix;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HipChat.Model
{
    public class Room : INotifyPropertyChanged
    {
        private string mName = null;
        private Jid mJid = null;
        public Chat chat;

        public bool IsPrivate;
        public string Topic;
        public int MembersCount = 0;
        public DateTime LastActive;

        private ObservableCollection<Contact> members;

        public event PropertyChangedEventHandler PropertyChanged;

        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }

        public string Name
        {
            get { return mName; }
            set
            {
                if (value != mName)
                {
                    mName = value;
                    NotifyPropertyChanged("Name");
                }
            }
        }

        public Jid Jid
        {
            get { return mJid; }
            set
            {
                if (value != mJid)
                {
                    mJid = value;
                    NotifyPropertyChanged("Jid");
                }
            }
        }

        public string DescriptionActivity
        {
            get
            {
                if (MembersCount == 0)
                {
                    return Topic != null ? Topic : "";
                    //return "No one is online";
                }

                string members = MembersCount == 1 ? "1 online member" : MembersCount + " online members";
                string active = DateHelper.GetPrettyDate(LastActive);

                if (active != null)
                {
                    members += ", active " + active;
                }
                return members;
            }
        }

        public string DescriptionOwner
        {
            get
            {
                return String.Format("{0} room", IsPrivate ? "Private" : "Public");
            }
        }

        public void AddMember(Contact person)
        {
            Members.Add(person);
        }

        public ObservableCollection<Contact> Members
        {
            get
            {
                if (members == null)
                {
                    members = new ObservableCollection<Contact>();
                    API.Current.GetRoomMembers(this);
                }

                return members;
            }
        }
    }
}
